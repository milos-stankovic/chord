var currency =  {
    "eur" : [
        {"chifer":"BCITITMM","bank":"INTESA SANPAOLO SPA","city":"MILANO, ITALY"}
    ],
    "usd" : [
        {"chifer":"BCITUS33","bank":"INTESA SANPAOLO SPA","city":"NEW YORK,NY,UNITED STATES"}
    ],
    "gbp" : [
        {"chifer":"BARCGB22","bank":"BARCLAYS BANK PLC","city":"LONDON, UNITED KINGDOM"}
    ]};
$(document).on( 'change', '#select', function(){

    if( $('select').val() == "1" ){
        $.each(currency.eur, function (key, item) {
            $('#chifer input').val(item.chifer);
            $('#bank input').val(item.bank);
            $('#city input').val(item.city);
        });
    }
    else if ( $('select').val() == "2" ) {
        $.each(currency.usd, function (key, item) {
            $('#chifer input').val(item.chifer);
            $('#bank input').val(item.bank);
            $('#city input').val(item.city);
        });
    }
    else if ( $('select').val() == "3" ) {
        $.each(currency.gbp, function (key, item) {
            $('#chifer input').val(item.chifer);
            $('#bank input').val(item.bank);
            $('#city input').val(item.city);
        });
    }
    else {
        $('#valuta1').html('select value');
        $('#valuta2').html('select value');
        $('#valuta3').html('select value');
    }
});



