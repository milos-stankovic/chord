// var photo_counter = 0;
// Dropzone.options.realDropzone = {
//
//     uploadMultiple: true,
//     parallelUploads: 100,
//     maxFilesize: 8,
//     previewsContainer: '#dropzonePreview',
//     previewTemplate: document.querySelector('#preview-template').innerHTML,
//     addRemoveLinks: true,
//     dictRemoveFile: 'Remove',
//     dictFileTooBig: 'Image is bigger than 8MB',
//
//     // The setting up of the dropzone
//     init:function() {
//
//         this.on("removedfile", function(file) {
//
//             $.ajax({
//                 type: 'POST',
//                 url: 'upload/delete',
//                 data: {id: file.name},
//                 dataType: 'html',
//                 success: function(data){
//                     var rep = JSON.parse(data);
//                     if(rep.code == 200)
//                     {
//                         photo_counter--;
//                         $("#photoCounter").text( "(" + photo_counter + ")");
//                     }
//
//                 }
//             });
//
//         } );
//     },
//     error: function(file, response) {
//         if($.type(response) === "string")
//             var message = response; //dropzone sends it's own error messages in string
//         else
//             var message = response.message;
//         file.previewElement.classList.add("dz-error");
//         _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
//         _results = [];
//         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
//             node = _ref[_i];
//             _results.push(node.textContent = message);
//         }
//         return _results;
//     },
//     success: function(file,done) {
//         photo_counter++;
//         $("#photoCounter").text( "(" + photo_counter + ")");
//     }
// }

Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element

    // The configuration we've talked about above
    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 100,
    maxFiles: 100,

    // The setting up of the dropzone
    init: function() {
        var myDropzone = this;

        // First change the button to actually tell Dropzone to process the queue.
        this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
            // Make sure that the form isn't actually being sent.
            e.preventDefault();
            e.stopPropagation();
            myDropzone.processQueue();
        });

        // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
        // of the sending event because uploadMultiple is set to true.
        this.on("sendingmultiple", function() {
            // Gets triggered when the form is actually being sent.
            // Hide the success button or the complete form.
        });
        this.on("successmultiple", function(files, response) {
            // Gets triggered when the files have successfully been sent.
            // Redirect user or notify of success.
        });
        this.on("errormultiple", function(files, response) {
            // Gets triggered when there was an error sending the files.
            // Maybe show form again, and notify user of error
        });
    }

}
