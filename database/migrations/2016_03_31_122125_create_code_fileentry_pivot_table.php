<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodeFileentryPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_fileentry', function (Blueprint $table) {
            $table->integer('code_id')->unsigned()->index();
            $table->foreign('code_id')->references('id')->on('codes')->onDelete('cascade');
            $table->integer('fileentry_id')->unsigned()->index();
            $table->foreign('fileentry_id')->references('id')->on('fileentries')->onDelete('cascade');
            $table->primary(['code_id', 'fileentry_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('code_fileentry');
    }
}
