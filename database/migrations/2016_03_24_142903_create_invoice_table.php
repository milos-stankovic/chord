<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project');
            $table->string('description');
            $table->string('notes');

            $table->string('intermediary_name');
            $table->string('intermediary_bank');
            $table->string('intermediary_location');

            $table->string('institution_name');
            $table->string('institution_bank');
            $table->string('institution_street');
            $table->string('institution_city');
            $table->string('institution_country');

            $table->integer('beneficiary_number');
            $table->string('beneficiary_company');
            $table->string('beneficiary_address');
            $table->string('beneficiary_city');

            $table->string('company_name');
            $table->string('company_address');
            $table->integer('company_zip');
            $table->string('company_city');
            $table->string('company_country');

            $table->integer('cost');
            $table->integer('amount_payable');
            $table->integer('total');
            $table->integer('subtotal');
            $table->integer('tax');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
