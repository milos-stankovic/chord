<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('address');
            $table->string('city');
            $table->integer('zip');
            $table->string('country');
            $table->string('phone');

            $table->string('company_name');
            $table->string('company_address');
            $table->integer('company_zip');
            $table->string('company_city');
            $table->string('company_country');
            $table->string('company_phone');
            $table->string('company_website');
            $table->string('company_email');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
