@extends('invoice.layout')
@section('content')
        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sample Invoice</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <style>
        @import url(http://fonts.googleapis.com/css?family=Bree+Serif);
        body, h1, h2, h3, h4, h5, h6{
            font-family: 'Bree Serif', serif;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-ls-12 logo">
            <div class="logo-img">
                    <img src="{{ asset('/images/logo.png') }}">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <h1>INVOICE</h1>
            <h1><small>Invoice #001</small></h1>
        </div>
        <div class="col-xs-6 text-right">
            <h1>15th March 2016</h1>
            <h1>Project: DRUCS</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <div class="panel panel-default">
                <div class="panel-heading from-to">
                    <h3>FROM:</h3>
                </div>
                <div class="panel-body">
                    <p>Chordate D.O.O</p>
                    <p> Beograd - Stari Grad,</p>
                    <p> Tadeusa Koscuska 12</p>
                    <p> Company #: 60430071</p>
                    <p> Tax #: 104716111 </p>
                    <p> oﬃce@chord.agency</p>
                </div>
            </div>
        </div>
        <div class="col-xs-5 col-xs-offset-2 text-right">
            <div class="panel panel-default">
                <div class="panel-heading from-to">
                    <h3>TO:</h3>
                </div>
                <div class="panel-body">
                    <p> {{ \Auth::user()->company_name }} </p>
                    <p>{{ \Auth::user()->company_address }}</p>
                    <p>{{ \Auth::user()->company_city }} {{ \Auth::user()->company_zip }}</p>
                    <p>{{ \Auth::user()->company_country }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- / end client details section -->
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>
                <h4>DESCRIPTION</h4>
            </th>
            <th>
                <h4>COST(€)</h4>
            </th>
            <th>
                <h4>AMOUNT PAYABLE (%):</h4>
            </th>
            <th>
                <h4>TOTAL:</h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Article</td>
            <td><a href="#">Title of your article here</a></td>
            <td class="text-right">-</td>
            <td class="text-right">$200.00</td>
        </tr>
        </tbody>
    </table>
    <div class="row text-right project-total">
        <div class="col-xs-2 col-xs-offset-8">
            <p>
                <strong>
                    Sub Total : <br>
                    TAX : <br>
                    Total : <br>
                </strong>
            </p>
        </div>
        <div class="col-xs-2">
            <strong>
                $1200.00 <br>
                N/A <br>
                $1200.00 <br>
            </strong>
        </div>
    </div>
    <div class="row amount">
        <div class="col-xs-7">
            <div class="span7">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>AMOUNT DETAILS</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <td>INTERMEDIARY:</td>
                                <td>INSTITUTION DETAILS:</td>
                                <td>BENEFICIARY:</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>BCITITMM</td>
                                <td>DBDBRSBG</td>
                                <td>RS35160005380001949022</td>
                            </tr>
                            <tr>
                                <td>INTESA SANPAOLO SPA</td>
                                <td>BANKA INTESA AD, BEOGRAD</td>
                                <td>CHORDATE DOO BEOGRAD</td>
                            </tr>
                            <tr>
                                <td>MILANO, ITALY</td>
                                <td>MILENTIJA POPOVICA 7B</td>
                                <td>TADEUSA KOSCUSKA 12</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>BEOGRAD, REPUBLIKA SRBIJA</td>
                                <td>BEOGRAD - STARI GRAD</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-5">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4>AMOUNT DUE</h4>
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <td>Subtotal</td>
                            <td>0</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Tax</td>
                            <td>N/A</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>TOTAL</td>
                            <td>0</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-7">
            <div class="span7">
                <div class="panel panel-info notes">
                    <div class="notes">
                        <h4>NOTES</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-5">
            <div class="signature">
                <div class="panel-heading">
                    <h4></h4>
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                        </tr>
                        <tr>

                        </tr>

                        <tr>

                            <td style="border-top: thin solid;"></td>
                        </tr>
                        </tbody>
                    </table>
                    <div style="text-align: center;"><h6>JACK GORDON HARRIS - MANAGING DIRECTOR</h6></div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
@endsection
