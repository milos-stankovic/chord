@extends('invoice.layout')
@section('content')
        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sample Invoice</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <style>
        @import url(http://fonts.googleapis.com/css?family=Bree+Serif);
        body, h1, h2, h3, h4, h5, h6{
            font-family: 'Bree Serif', serif;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-ls-12 logo">
            <div class="logo-img">
                <img src="{{ asset('/images/logo.png') }}">
            </div>
        </div>
    </div>
    <form action="{{ route('invoice.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-xs-6">
                <h1>INVOICE</h1>
                <h1><small>Invoice #001</small></h1>
            </div>
            <div class="col-xs-6 text-right">
                <h1>{{ date('dS F o') }}</h1>
                <h1>Project: <input name="project" type="text" placeholder="Enter project name"></h1></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <div class="panel panel-default">
                    <div class="panel-heading from-to">
                        <h3>FROM:</h3>
                    </div>
                    <div class="panel-body">
                        <p>Chordate D.O.O</p>
                        <p> Beograd - Stari Grad,</p>
                        <p> Tadeusa Koscuska 12</p>
                        <p> Company #: 60430071</p>
                        <p> Tax #: 104716111 </p>
                        <p> oﬃce@chord.agency</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-5 col-xs-offset-2 text-right">
                <div class="panel panel-default">
                    <div class="panel-heading from-to">
                        <h3>TO:</h3>
                    </div>
                    <div class="panel-body">
                        <p> {{ \Auth::user()->company_name }} </p>
                        <p>{{ \Auth::user()->company_address }}</p>
                        <p>{{ \Auth::user()->company_city }} {{ \Auth::user()->company_zip }}</p>
                        <p>{{ \Auth::user()->company_country }}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- / end client details section -->
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>
                    <h4>DESCRIPTION</h4>
                </th>
                <th>
                    <h4>COST<select id="select">
                            <option value="1">(€)</option>
                            <option value="2">($)</option>
                            <option value="3">(£)</option>
                        </select></h4>
                </th>
                <th>
                    <h4>AMOUNT PAYABLE (%):</h4>
                </th>
                <th>
                    <h4>TOTAL:</h4>
                </th>
            </tr>
            </thead>
            <tbody class="plusrow">
            <tr>
                <td><h4><input name="description" type="text" placeholder="Describe it.."></h4></td>
                <td><h4><input name="cost" type="text" id="cost" placeholder="Cost"></h4></td>
                <td><h4><input name="amount_payable" type="text" id="percent" placeholder="Amount Payable"></h4></td>
                <td><h4><input name="total" type="text" id="subtotal" placeholder="Total"></h4></td>
            </tr>
            </tbody>
        </table>
        <div class="row text-right project-total">
            <div class="col-xs-2 col-xs-offset-8">

            </div>
            <div class="col-xs-2">
                <button type="button" class="btn btn-primary plusbtn"><span class="glyphicon glyphicon-plus"></span></button>
                <button type="button" class="btn btn-primary minusbtn"><span class="glyphicon glyphicon-minus"></span></button>
            </div>
        </div>
        <div class="row amount">
            <div class="col-xs-7">
                <div class="span7">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4>ACCOUNT DETAILS</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped account">
                                <thead>
                                <tr>
                                    <td>INTERMEDIARY:</td>
                                    <td>INSTITUTION DETAILS:</td>
                                    <td>BENEFICIARY:</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td id="chifer"><input name="intermediary_name" value="BCITITMM" readonly></td>
                                    <td>DBDBRSBG</td>
                                    <td>RS35160005380001949022</td>
                                </tr>
                                <tr>
                                    <td id="bank"><input name="intermediary_bank" value="INTESA SANPAOLO SPA" readonly></td>
                                    <td>BANKA INTESA AD, BEOGRAD</td>
                                    <td>CHORDATE DOO BEOGRAD</td>
                                </tr>
                                <tr>
                                    <td id="city"><input name="intermediary_location" value="MILANO, ITALY" size="26" readonly></td>
                                    <td>MILENTIJA POPOVICA 7B</td>
                                    <td>TADEUSA KOSCUSKA 12</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>BEOGRAD, REPUBLIKA SRBIJA</td>
                                    <td>BEOGRAD - STARI GRAD</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-5">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>AMOUNT DUE</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped account">
                            <thead>
                            <tr>
                                <td>Subtotal</td>
                                <td><input id="Subtotal" name="subtotal" readonly></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tax</td>
                                <td><input id="tax" type="text" name="tax"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>TOTAL</td>
                                <td><input id="total" type="text" name="total" readonly></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-7">
                <div class="span7">
                    <div class="panel panel-info notes">
                        <div class="notes">
                            <h4>NOTES</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-5">
                <div class="signature">
                    <div class="panel-heading">
                        <h4></h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>

                            </tr>
                            <tr>

                            </tr>

                            <tr>

                                <td style="border-top: thin solid;"></td>
                            </tr>
                            </tbody>
                        </table>
                        <div style="text-align: center;"><h6>JACK GORDON HARRIS - MANAGING DIRECTOR</h6></div>
                        <input type="submit" class="btn btn-info invoice" value="Submit Button">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>
@endsection
