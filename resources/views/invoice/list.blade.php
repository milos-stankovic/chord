@extends('invoice.admin.admin')

@section('sidebar')
    <div class="row second-header">
        <div class="col-lg-12 ">
            <div class="col-lg-12 from">
                <div class="col-lg-12 text-left description">
                    {{--<h4>From:</h4>--}}
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <td><h4>Invoice #</h4></td>
                            <td><h4>Client Name</h4></td>
                            <td><h4>Project</h4></td>
                            <td><h4>Amount</h4></td>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead>
                        @foreach ($invoices as $invoice)
                            @foreach ($invoice->users as $users)
                                <tr>
                                    <td>{{$invoice->id}}</td>
                                    <td>{{$users->name}}</td>
                                    <td>{{$invoice->description}}</td>
                                    <td>{{$invoice->total}}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
