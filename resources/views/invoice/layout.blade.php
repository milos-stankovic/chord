<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Invoice</title>

    <!-- Bootstrap Core CSS -->
    {{--<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">--}}

    <!-- Custom CSS -->
    <link href="{{asset('css/admin.css')}}" rel="stylesheet">
    {{--<link href="{{asset('css/invoice.css')}}" rel="stylesheet">--}}
    <link href="{{asset('css/upload.css')}}" rel="stylesheet">
    <link href="{{asset('css/test.css')}}" rel="stylesheet">
    {{--<link href="{{asset('css/downloads.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ public_path().'/css/test.css' }}">


    <style type="text/css">
        .header{
            background-color: #843534;
            height: auto;
            padding: 10px; !important;
        }

        .header-text p{
            text-align: right;
            padding: 0;
            margin: 0;
            color: #fff;
        }

        .container2{
            border: 1px solid #8f9497; !important;
        }

        .htext{
            color: #675F5A;
        }

        .down-header h3{
            padding: 0;
            margin: 0;
            padding-bottom: 5px;
        }

        .second-header{
            margin-top: 20px;
            margin-bottom: 10px;
            padding-left: 15px; !important;
            padding-right: 15px; !important;
        }

        .from{
            border: 2px solid #856C66;
            border-radius: 5px;
            padding: 2px; !important;
        }

        .account{
            border: 2px solid #856C66;
            border-radius: 5px;
            padding: 0; !important;
        }

        .to{
            border: 2px solid #856C66;
            border-radius: 5px;
            padding: 2px; !important;
        }

        .from-header{
            background-color: #856C66;
            border-top-left-radius: 5px; !important;
            border-top-right-radius: 5px; !important;
            margin-bottom: 5px; !important;

        }
        .table{
            margin-bottom: 0px; !important;
        }
        .from-header h4{
            color: #fff;
        }

        .from-text > p{
            background-color: #EEEDED; !important;
            padding: 0 0 0 5px; !important;
            margin: 0;
            margin-bottom: 3px;
            color: #807874; !important;
        }

        .description{
            background-color: #C47B5D;
            border-top-left-radius: 5px; !important;
            border-top-right-radius: 5px; !important;
            margin-bottom: 5px; !important;
        }

        .description h4{
            color: #fff;
        }

        .description2{
            background-color: #E6BE73;
            border-top-left-radius: 2px; !important;
            border-top-right-radius: 2px; !important;
            margin-bottom: 5px; !important;
        }

        .description2 h4{
            color: #fff;
        }
        input.btn.btn-info.invoice {
            float: right;
            margin: 50px 100px;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
    <![endif]-->



</head>

<body>
@yield('content')

<!-- /.container -->

<!-- jQuery -->
<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/calculations.js')}}"></script>
<script src="{{asset('js/tableplus.js')}}"></script>
<script src="{{asset('js/upload.js')}}"></script>
<script src="{{asset('js/zip.js')}}"></script>
<script src="{{asset('js/accounts_change.js')}}"></script>


<!-- Bootstrap Core JavaScript -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>


</body>

</html>
