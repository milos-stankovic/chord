{{--@extends('invoice.layout')--}}
{{--@section('content')--}}
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Invoice</title>

    <!-- Bootstrap Core CSS -->
    {{--<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">--}}

    <!-- Custom CSS -->
    {{--<link href="{{asset('css/logo-nav.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">--}}

    {{--<link href="{{asset('css/invoice.css')}}" rel="stylesheet">--}}
    <link href="/var/www/public/chord/public/css/invoice.css" rel="stylesheet">


    {{--<link href="{{asset('css/sb-admin-rtl.css')}}" rel="stylesheet">--}}

    <style type="text/css">

    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
    <![endif]-->



</head>

<body>
        <!-- Navigation -->

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                {{--<img src="http://placehold.it/150x50&text=Logo" alt="">--}}
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container container2 ">
    <div class="row">
        <div class="col-lg-12 header">
            <div class="col-lg-6">
                {{--<img src="http://placehold.it/150x50&text=Logo" width="200px" alt="">--}}
            </div>
            <div class="col-lg-6">
                <header class="header-text">
                    <p>Chordate D.O.O</p>
                    <p> Beograd - Stari Grad,</p>
                    <p> Tadeusa Koscuska 12</p>
                    <p> Company #: 60430071</p>
                    <p> Tax #: 104716111 </p>
                    <p> oﬃce@chord.agency</p>
                </header>
            </div>
        </div>
    </div>

    <div class="row second-header">
        <div class="col-lg-12">
            <div class="col-lg-6 htext down-header">
                <h3>Invoice payable</h3>
                <h3>PO #: N / A</h3>
            </div>
            <div class="col-lg-6 htext text-right down-header">
                <h3>15th March 2016</h3>
                <h3>Project: DRUCS</h3>
            </div>
        </div>
    </div>

    <div class="row second-header">
        <div class="col-lg-12 ">
            <div class="col-lg-6 from">
                <div class="col-lg-12 text-left from-header">
                    <h4>From:</h4>
                </div>
                <div class="col-lg-5">
                    {{--<img src="http://placehold.it/150x50&text=Logo" alt="">--}}
                </div>
                <div class="col-lg-7 from-text">
                    <p>Chordate D.O.O</p>
                    <p>Tadeusa Koscuska 12</p>
                    <p>Beograd - Stari Grad 11000</p>
                    <p>Serbia</p>
                </div>
            </div>
            <div class="col-lg-6 to">
                <div class="col-lg-12 text-left from-header">
                    <h4>To:</h4>
                </div>
                <div class="col-lg-7 from-text">
                    <p>Chordate D.O.O</p>
                    <p>Tadeusa Koscuska 12</p>
                    <p>Beograd - Stari Grad 11000</p>
                    <p>Serbia</p>
                </div>
                <div class="col-lg-5">
                    {{--<img src="http://placehold.it/150x50&text=Logo" alt="">--}}
                </div>

            </div>
        </div>
    </div>

    <div class="row second-header">
        <div class="col-lg-12 ">
            <div class="col-lg-12 from">
                <div class="col-lg-12 text-left description">
                    {{--<h4>From:</h4>--}}
                    <table class="table table-striped">
                    <thead>
                    <tr>
                    <td><h4>Description</h4></td>
                    <td><h4>Cost</h4></td>
                    <td><h4>Amount Payable</h4></td>
                    <td><h4>Total</h4></td>
                    </tr>
                    </thead>
                    </table>
                </div>
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <td>Description</td>
                            <td>Cost</td>
                            <td>Amount Payable</td>
                            <td>Total</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>John</td>
                            <td>Doe</td>
                            <td>john@example.com</td>
                            <td>Total</td>
                        </tr>
                        <tr>
                            <td>Mary</td>
                            <td>Moe</td>
                            <td>mary@example.com</td>
                            <td>Total</td>
                        </tr>
                        <tr>
                            <td>July</td>
                            <td>Dooley</td>
                            <td>july@example.com</td>
                            <td>Total</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row second-header">
        <div class="col-lg-12 ">
            <div class="col-lg-8 account">
                <div class="col-lg-12 text-center description2">
                    <h4>ACCOUNT DETAILS:</h4>
                </div>
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <td>INTERMEDIARY:</td>
                            <td>INSTITUTION DETAILS:</td>
                            <td>BENEFICIARY:</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>BCITITMM</td>
                            <td>DBDBRSBG</td>
                            <td>RS35160005380001949022</td>
                        </tr>
                        <tr>
                            <td>INTESA SANPAOLO SPA</td>
                            <td>BANKA INTESA AD, BEOGRAD</td>
                            <td>CHORDATE DOO BEOGRAD</td>
                        </tr>
                        <tr>
                            <td>MILANO, ITALY</td>
                            <td>MILENTIJA POPOVICA 7B</td>
                            <td>TADEUSA KOSCUSKA 12</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>BEOGRAD, REPUBLIKA SRBIJA</td>
                            <td>BEOGRAD - STARI GRAD</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-lg-4 account">
                <div class="col-lg-12 text-center description2">
                    <h4>AMOUNT DUE:</h4>
                </div>
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <td>Subtotal</td>
                            <td>0</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Tax</td>
                            <td>N/A</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>TOTAL</td>
                            <td>0</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{--@endsection--}}
        <!-- /.container -->

        <!-- jQuery -->
        <script src="{{asset('js/jquery.js')}}"></script>
        <script src="{{asset('js/dropme.js')}}"></script>
        <script src="{{asset('js/tableplus.js')}}"></script>
        <script src="{{asset('js/upload.js')}}"></script>
        <script src="{{asset('js/zip.js')}}"></script>
        <script src="{{asset('/packages/dropzone/dropzone.js')}}"></script>
        <script src="{{asset('/packages/dropzone/dropzone-config.js')}}"></script>

        <!-- Bootstrap Core JavaScript -->
        {{--<script src="{{asset('js/bootstrap.min.js')}}"></script>--}}


</body>

</html>
