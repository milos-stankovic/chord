@extends('invoice.layout')
@section('content')
    <div class="bs-example">
        <form action="{{ route('invoice.store') }}" method="POST">

            <div class="form-group">
                <h4>TO: <select id="client">
                        <option value="1">CLIENT #X</option>
                        <option value="2">CLIENT #X</option>
                        <option value="3">CLIENT #X</option>
                    </select></h4>
            </div>

            <div class="form-group">
                <h4>COST: <select id="select">
                        <option value="1">(€)</option>
                        <option value="2">($)</option>
                        <option value="3">(£)</option>
                    </select></h4>
            </div>

            <div class="form-group">
                <label for="project">Project</label>
                <input type="text" class="form-control" id="project" placeholder="Project">
            </div>

            <div class="form-group">
                <label for="client">P/O Number</label>
                <input type="text" class="form-control" id="P/O" placeholder="P/O">
            </div>

            <div class="form-group">
                <table responsive-table>
                    <tbody class="plusrow">
                    <tr>
                        <td><input name="description" type="text" class="form-control" placeholder="Describe it.."></td>
                        <td><input name="cost" type="text" id="cost" class="form-control" placeholder="Cost"></td>
                        <td><input name="amount_payable" type="text" id="percent" class="form-control" placeholder="Amount Payable"></td>
                        <td><input name="total" type="text" id="subtotal" class="form-control" placeholder="SubTotal"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="form-group">
                <label for="tax">Tax</label>
                <input id="tax" class="form-control" placeholder="Tax" type="text" name="tax">
                <label for="total">TOTAL</label>
                <input id="total" class="form-control" placeholder="Total" type="text" name="total">
            </div>
            <div class="row text-right project-total">
                <div class="col-xs-2 col-xs-offset-8">
                </div>
                <div class="col-xs-2">
                    <button type="button" class="btn btn-primary plusbtn"><span class="glyphicon glyphicon-plus"></span>
                    </button>
                    <button type="button" class="btn btn-primary minusbtn"><span
                                class="glyphicon glyphicon-minus"></span></button>
                </div>
            </div>
            <div class="form-group">
                <label for="comment">Notes:</label>
                <textarea class="form-control" rows="5" id="comment"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection
