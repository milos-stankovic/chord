@extends('invoice.layout')
{{--<h1 class="files white"> Download your files: </h1>--}}

{{--@foreach($codes as $files)--}}
{{--@foreach($files->fileentry as $file)--}}

    {{--<div class="row">--}}
            {{--<ul class="thumbnails">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-lg-3 col-md-4 col-xs-6 thumb">--}}
                        {{--<a class="thumbnail" href="{{route('downloadentry', $file->filename)}}">--}}
                            {{--<img class="img-responsive" src="{{route('getentry', $file->filename)}}" alt="ALT NAME">--}}
                        {{--</a>--}}
                        {{--<div class="caption">--}}
                            {{--<a href="{{route('downloadentry', $file->filename)}}">--}}
                                {{--<p>{{$file->original_filename}}</p></a>--}}
                            {{--<input class="cbox" type="checkbox" name="files[]"--}}
                                   {{--value="{{$file->original_filename}}"/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                    {{--@endforeach--}}
                {{--</div>--}}
            {{--</ul>--}}
            {{--</form>--}}
    {{--</div>--}}
<style>

    body {
        background: #ecf0f1;
        font: 16px Helvetica;
    }

    section {
        width: 325px;
        margin: 20px auto;
        padding: 20px 0;
        border: 1px solid #ebebeb;
        border-radius: 5px;
        background: white;
    }

    h1 {
        padding: 0 0 20px 20px;
        font-size: 2em;
        font-weight: 100;
        color: #333;
    }

    h2 {
        padding: 10px 20px;
        border-top: 1px solid #ebebeb;
        border-bottom: 1px solid #ebebeb;
        color: #333;
    }
    h2 span {
        font-weight: 100;
        color: #999;
    }

    a {
        display: block;
        height: 75px;
        line-height: 75px;
        padding: 0 20px;
        border-bottom: 1px solid #ebebeb;
        color: #333;
        text-decoration: none;
        background: #f8f8f8;
        transition: background ease-in-out .2s;
    }
    a:hover {
        background: #dedede;
    }
    a span {
        display: inline-block;
        width: 50px;
        height: 50px;
        line-height: 50px;
        margin-right: 10px;
        padding: 2px;
        border-radius: 50%;
        color: white;
        font-size: 14px;
        text-align: center;
    }
    a .html {
        background: #2980b9;
    }
    a .css {
        background: #e67e22;
    }

    .owner h2 {
        padding: 20px 0 10px 20px;
        border: none;
        color: #333;
    }
    .owner p {
        padding: 0 20px;
        color: #999;
        font-weight: 100;
    }

    button {
        display: block;
        width: 285px;
        height: 50px;
        margin: 20px auto;
        border: none;
        border-radius: 5px;
        background: #1dd2af;
        color: white;
        font-size: 16px;
        font-weight: 100;
        transition: background ease-in-out .2s;
    }
    button:hover {
        background: #1abc9c;
    }
    button:focus {
        outline: none;
    }

</style>

    <section>
        <h1>Welcome</h1>
        <h2>
            <span>Click for download:</span>
        </h2>
                <ul>
                    @foreach($codes as $files)
                        @foreach($files->fileentry as $file)
                    <li>
                        <a href="{{route('downloadentry', $file->filename)}}">
                            <span class='html'>File</span>
                            {{$file->original_filename}}
                        </a>

                    </li>
                        @endforeach
                    @endforeach
                </ul>
                {{--<div class='owner'>--}}
                    {{--<h2>Or download zip:</h2>--}}
                    {{--<p>zip file link here..</p>--}}
                {{--</div>--}}

                {{--<button>DOWNLOAD</button>--}}


    </section>




