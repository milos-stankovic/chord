@extends('invoice.admin.admin')

@section('sidebar')
    <div class="row second-header">
        <div class="col-lg-12 ">
            <div class="col-lg-12 from">
                <div class="col-lg-12 text-left description">
                    {{--<h4>From:</h4>--}}
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <td><h4>Archive #</h4></td>
                            <td><h4>Archive Name</h4></td>
                            <td><h4>Code</h4></td>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead>
                        @foreach ($archives as $archive)
                            {{--@foreach ($invoice->users as $users)--}}
                                <tr>
                                    <td>{{$archive->id}}</td>
                                    <td>{{$archive->name}}</td>
                                    <td>{{$archive->code}}</td>
                                </tr>
                            {{--@endforeach--}}
                        @endforeach
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
