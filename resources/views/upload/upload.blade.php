@extends('invoice.admin.admin')

@section('sidebar')

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    {{--<body>--}}
    <div class="container gallerycontainer">
        <div class="row" id="car">
            <div class="col-md-12">
                <h1 class="white">Upload File Management</h1>
                <p class="white">Your archive will be stored at storage/archives folder.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 center">
                <div class="btn-container">
                    <!--the three icons: default, ok file (img), error file (not an img)-->
                    <h1 class="imgupload"><i class="fa fa-file-image-o"></i></h1>
                    <h1 class="imguploadok"><i class="fa fa-check"></i></h1>
                    <h1 class="imguploadstop"><i class="fa fa-times"></i></h1>
                    <!--this field changes dinamically displaying the filename we are trying to upload-->
                    <p id="namefile">You can upload multiple files at once.</p>
                    <!--our custom btn which which stays under the actual one-->
                    <form role="form" action="{{route('addentry', [])}}" method="post" enctype="multipart/form-data"
                          id="uploadForm">
                        {{ csrf_field() }}
                        <button type="button" id="btnup" multiple="true" class="btn btn-primary btn-lg">Browse your
                            files
                        </button>
                        <!--this is the actual file input, is set with opacity=0 beacause we wanna see our custom one-->
                        <input type="file" multiple="true" value="" name="filefield" id="fileup">
                    </form>
                </div>
            </div>
            <!--additional fields-->
            <form id="zipName" role="form" action="{{route('multipleselect')}}" method="post">
                {{ csrf_field() }}
                <div class="col-md-6">
                    <div class="row">
                        <div class="form-group" id="top">
                            <div class="col-md-12">
                                <input type="text" maxlength="50" class="form-control" name="archive_name" id="tit"
                                       placeholder="Archive Name">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <p class="white">All fields are mandatory</p>
                        </div>
                        <div class="col-md-4">
                            <!--the defauld disabled btn and the actual one shown only if the three fields are valid-->
                            <input type="submit" form="gallery" value="Submit!" class="btn btn-primary" id="bottone"
                                   style="padding-left:50px; padding-right:50px; display:none;">
                            <button type="submit" class="btn btn-default" id="bottone"
                                    style="padding-left:40px; padding-right:40px;"> Zip!
                            </button>
                        </div>
                    </div>
                </div>

        </div>


        {{--FILE GALERY--}}

        <h1 class="files white"> Media Library</h1>
        @foreach($entries as $entry)
            <div class="row">
                {{--<form role="form" action="{{route('multipleselect')}}" method="post" id="gallery">--}}
                {{--{{ csrf_field() }}--}}
                <ul class="thumbnails">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                            <a class="thumbnail" href="{{route('downloadentry', $entry->filename)}}">
                                <img class="img-responsive" src="{{route('getentry', $entry->filename)}}"
                                     alt="ALT NAME">
                            </a>
                            <div class="caption">
                                <a href="{{route('downloadentry', $entry->filename)}}">
                                    <p>{{$entry->original_filename}}</p></a>
                                <input class="cbox" type="checkbox" name="files[]"
                                       value="{{$entry->original_filename}}"/>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </ul>
                </form>
            </div>

            {{--FILE GALERY END --}}
    </div>
    {{--</body>--}}

@stop
