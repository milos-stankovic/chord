<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceUser extends Model
{
    protected $table = 'invoice_user';
    protected $fillable = ['user_id', 'invoice_id'];

    public function getUsers()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
