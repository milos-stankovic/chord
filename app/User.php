<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password', 'address', 'city', 'zip', 'country',
        'phone', 'company_name', 'company_city', 'company_address', 'company_zip',
        'company_country', 'company_phone', 'company_email', 'company_website'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function invoices()
    { // looking for groups user has.
        //('manyToMany Model', 'PivotModel','thisModelPivotId','local id of model you searching')
//        return $this->hasManyThrough('App\Invoice','App\InvoiceUser', 'user_id', 'user_id');
        return $this->belongsToMany('App\Invoice');

    }
}
