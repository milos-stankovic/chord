<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $fillable = [
        'description',
//        'notes',
        'institution_name',
        'intermediary_name',
        'intermediary_bank',
        'intermediary_location',
        'institution_bank',
        'institution_street',
        'institution_city',
        'institution_country',
        'beneficiary_number',
        'beneficiary_company',
        'beneficiary_address',
        'beneficiary_city',
        'company_name',
        'company_address',
        'company_zip',
        'company_city',
        'company_country',
        'cost',
        'amount_payable',
        'total',
        'subtotal',
        'tax',
        'project'
    ];

    public function users()
    { // looking for groups user has.
        //('manyToMany Model', 'PivotModel','thisModelPivotId','local id of model you searching')
//        return $this->hasManyThrough('App\User','App\InvoiceUser','invoice_id', 'id');
        return $this->belongsToMany('App\User');

    }
}
