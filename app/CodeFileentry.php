<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeFileentry extends Model
{
    protected $table = 'code_fileentry';
    protected $fillable = ['code_id', 'fileentry_id'];
    //
}
