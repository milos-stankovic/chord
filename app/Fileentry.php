<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fileentry extends Model
{
    protected $table = 'fileentries';
    protected $fillable = [];

    public function code()
    {
        return $this->belongsToMany('App\Code');
    }
}
