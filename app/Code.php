<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $table = 'codes';
    protected $fillable = ['id','code','name','active','downloads','counter'];
    public function fileentry()
    {
        return $this->belongsToMany('App\Fileentry');
    }
}
