<?php

namespace App\Http\Controllers;

use App\Code;
use App\Fileentry;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function codeCheck(Request $request,Code $code)
    {

        if($codes = Code::with('fileentry')->where('code',$request->get('code'))->get())
        {
            return view('download',compact('codes'));
        }

    }
}
