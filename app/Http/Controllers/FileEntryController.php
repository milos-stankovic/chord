<?php

namespace App\Http\Controllers;

use App\Code;
use App\Fileentry;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;


class FileEntryController extends Controller
{
    /**
     * @return mixed
     * Show all fileentires in Media Gallery.
     */
    public function index()
    {
        $entries = Fileentry::all();

        return view('upload.upload', compact('entries'));
    }

    /**
     * @return mixed
     * Upload files to folder/server.
     */
    public function add()
    {
        $file = \Request::file('filefield');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getFilename() . '.' . $extension, File::get($file));
        $entry = new Fileentry();
        $entry->mime = $file->getClientMimeType();
        $entry->original_filename = $file->getClientOriginalName();
        $entry->filename = $file->getFilename() . '.' . $extension;
        $entry->save();

//        $code = $entry->code()->create(['code' => bcrypt(str_random(60))]);
//        $entry->code()->sync([$code->id], false);

        return back();

    }

    /**
     * @param $filename
     *
     * @return $this
     * Download file.
     */
    public function get($filename)
    {
        $entry = Fileentry::where('filename', '=', $filename)->first();
        $file = Storage::disk('local')->get($entry->filename);
        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime);

    }

    /**
     * @param $filename
     *
     * @return mixed Download selected?
     * Download selected?
     * @internal param $original_filename
     *
     */
    public function download($filename)
    {
        $entry = Fileentry::where('filename', '=', $filename)->first();
        $file = Storage::disk('local')->get($entry->filename);
        //$path  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        //*if extension is .zip, if not this is true*//
        $path = public_path() . "/uploads/" . $entry->filename;
        return response()->download($path);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed Zip selected files with checkboxes.
     * Zip selected files with checkboxes.
     */
    public function zipFiles(Request $request)
    {
        $selected_files = $request->get('files');
        $code = bcrypt(str_random(60));
        foreach ($selected_files as $sf) {
            $files[] = public_path() . "/uploads/" . Fileentry::where('original_filename', '=', $sf)->first()->filename;
            $protect = Fileentry::where('original_filename', '=', $sf)->first();
            $protect->code()->create(['code' => $code, 'name' => $request->get('archive_name')]);
            $protect->code()->sync([$protect->id], false);
            //$protect->code()->attach($protect);
        }
        \Zipper::zip(public_path() . "/uploads/archives/" . $request->get('archive_name') . ".zip")->add($files);
        return back();
    }

    public function archiveList(Code $code)
    {
        $archives = $code->all();
        return view('upload.archives_list', compact('archives'));
    }


}
