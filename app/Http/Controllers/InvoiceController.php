<?php

namespace App\Http\Controllers;

use App\InvoiceUser;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Invoice;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;


class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::with('users')->get();
        return view('invoice.list', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('invoice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request     *
     */
    public function store(Request $request)
    {
//        $request->except(['created_at', 'updated_at']);
        $invoice = new Invoice();

        $invoice->project = $request->input('project');
        $invoice->description = $request->input('description');
//        $invoice->notes = $request->input('notes');
        $invoice->intermediary_name = $request->input('intermediary_name');
        $invoice->intermediary_bank = $request->input('intermediary_bank');
        $invoice->intermediary_location = $request->input('intermediary_location');
//        $invoice->institution_name = $request->input('institution_name');
//        $invoice->institution_bank = $request->input('institution_bank');
//        $invoice->institution_street = $request->input('institution_street');
//        $invoice->institution_city = $request->input('institution_city');
//        $invoice->institution_country = $request->input('institution_country');
//        $invoice->beneficiary_number = $request->input('beneficiary_number');
//        $invoice->beneficiary_company = $request->input('beneficiary_company');
//        $invoice->beneficiary_address = $request->input('beneficiary_address');
//        $invoice->beneficiary_city = $request->input('beneficiary_city');
//        $invoice->company_name = $request->input('beneficiary_company');
//        $invoice->company_address = $request->input('beneficiary_address');
//        $invoice->company_zip = $request->input('beneficiary_zip');
//        $invoice->company_city = $request->input('beneficiary_city');
//        $invoice->company_country = $request->input('beneficiary_country');
        $invoice->amount_payable = $request->input('amount_payable');
        $invoice->cost = $request->input('cost');
        $invoice->total = $request->input('total');
        $invoice->subtotal = $request->input('subtotal');
        $invoice->tax = $request->input('tax');
        $invoice->save();

        $pivot = new InvoiceUser;
        $pivot->invoice_id =  $invoice->id;
        $pivot->user_id =  \Auth::user()->id;
        $pivot->save();
        return redirect()->route('invoice.export',$invoice->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = \Auth::user()->invoices()->first();
        return view('invoice.user_invoice', compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function export($id)
    { // daj mu id u argumentu a dole u invoice da nadje po tom id-u pdf......
//        $invoice =  \Auth::user()->invoices()->first();
//        $html = (string) view('invoice.user_invoice');
//        dd($html);
        // for download (in public folder):
//        $data = [];
//        $pdf = PDF::loadView('invoice.test', $data);
//        $pdf->save('invoice.pdf');
//        return \Response::download('invoice.pdf');

        // for stream:

//        $invoice =  \Auth::user()->invoices()->first();
        $invoice =  Invoice::find($id);
        $pdf = PDF::loadView('invoice.user_invoice', array('invoice' => $invoice));
        return $pdf->stream('invoice.pdf');

//        \Excel::create('Pedeef', function($excel) {
//            $excel->sheet('pdfff', function($sheet) {
//                $sheet->setAutoSize(true);
//                $sheet->loadView('invoice.test');
//            });
//        })->export('pdf');


    }


}
