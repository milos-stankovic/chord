<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function () {

    /** Scaffold Auth Routes **/
    Route::auth();
    /** End Of Scaffold Auth Routes **/

    /** Place to enter a code. **/
    Route::get('/download', 'HomeController@index')->name('home');

    Route::post('/download/codeCheck', 'HomeController@codeCheck')->name('codecheck');
    /** End of Place to enter a code. **/

    /** Upload and Zip Files . **/
    Route::get('upload', 'FileEntryController@index')->name('fileentry');

    Route::get('upload/get/{filename}', [
        'as' => 'getentry', 'uses' => 'FileEntryController@get']);

    Route::post('upload/add',[
        'as' => 'addentry', 'uses' => 'FileEntryController@add']);

    Route::get('upload/download{filename}',[
        'as' => 'downloadentry', 'uses' => 'FileEntryController@download']);

    Route::post('upload/zip',[
        'as' => 'multipleselect', 'uses' => 'FileEntryController@zipFiles']);

    Route::post('upload/zipName',[
        'as' => 'zipname', 'uses' => 'FileEntryController@archiveName']);

    Route::get('upload/archives',[
        'as' => 'archives', 'uses' => 'FileEntryController@archiveList']);
    /** End Of Upload and Zip Files **/

    /** Invoice Routes**/
    Route::get('invoice/export/{id}', 'InvoiceController@export')->name('invoice.export');

    Route::resource('invoice', 'InvoiceController');
    /** End Of Invoice Routes**/

});


